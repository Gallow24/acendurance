import math

class Tria(object):
    """description of class"""
    def __init__(self, p1x, p1z, p2x, p2z, p3x, p3z):
        # concept: pit parted into triangles and we simply check if the car position is in one of them
        # Punkte aus pitpoints.xml
        self.p1x = p1x
        self.p1z = p1z
        self.p2x = p2x
        self.p2z = p2z
        self.p3x = p3x
        self.p3z = p3z

    def __repr__(self):
        return (f"{self.__class__.__name__}("
                f"p1={self.p1x}/{self.p1z} - p2={self.p2x}/{self.p2z} - p3={self.p3x}/{self.p3z})")
        

    def point_in_tria(self, px, pz):
        sgn1 = Tria.angle_sign(self.p1x, self.p1z, self.p2x, self.p2z, px, pz)
        sgn2 = Tria.angle_sign(self.p2x, self.p2z, self.p3x, self.p3z, px, pz)
        sgn3 = Tria.angle_sign(self.p3x, self.p3z, self.p1x, self.p1z, px, pz)
        
        if sgn1 == sgn2 == sgn3:
            return True
        else:
            return False

    @staticmethod
    def angle_sign(xa, za, xb, zb, xp, zp):
        sgn = (zb-zp)*(xa-xp) - (za-zp)*(xb-xp)
        if sgn >= 0:
            return 1
        else:
            return -1
