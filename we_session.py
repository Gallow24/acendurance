import datetime
class WeSession(object):
    def __init__(self):
        self.type = 1
        self.active = False
        self.active_since = datetime.datetime.now()
        self.start_timestamp = datetime.datetime.now()

    def __repr__(self):
        return (f"{self.__class__.__name__}("
                f"{self.type}, {self.active}, {self.active_since}, {self.start_timestamp})")
        
    def Refresh_Session_Active(self):
        Session_Offset = 0
        # wenn Renn- Session: 5 Sekunden vor Ablauf der Wartezeit springt das Auto an die Box. Damit ist in diesem Sinne die Session aktiv.
        if self.type == 3:
            Session_Offset = -5
        if datetime.datetime.now() >= self.start_timestamp + datetime.timedelta(0, Session_Offset):
            self.active = True
        else:
            self.active = False
