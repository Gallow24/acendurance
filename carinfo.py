import config_reader as cr

CONFIGFILE = "./config/cars.xml"
# constants with D_ prefix are dictionary keys
D_NAME  = "name"
D_RACECLASS = "class"

class CarInfo(object):
    def __init__(self, carname = "", raceclass=""):
        self.CarName     = carname
        self.RaceClass   = raceclass

    def __repr__(self):
        return (f"{self.__class__.__name__}("
                f"{self.CarName}, {self.RaceClass})")

    
    @staticmethod
    def get_cars_dict(): # create a dict with car name as key and carinfo-object as value
        Arr = cr.ConfigReader.get_dictarray_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_car())
        Dict = {}
        for cardef in Arr:
            Dict[cardef[D_NAME]] = CarInfo(cardef[D_NAME], cardef[D_RACECLASS])
        return Dict
