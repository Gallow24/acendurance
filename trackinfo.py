import tria
import config_reader as cr

CONFIGFILE = "./config/tracks.xml"

class Trackinfo(object):
    def __init__(self):
        self.track = ""
        self.layout = ""
        self.PitSpeedLimit = 0
        self.ptria = [] #speichere Dreicke
        
    def __repr__(self):
        return (f"{self.__class__.__name__}("
                f"{self.track}, {self.layout}, {self.PitSpeedLimit}, "
                f"{len(self.ptria)})")

    def gen_trias(self):
            if self.track != "":
                mydict = cr.ConfigReader.get_dict_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_track(self.track))
                self.PitSpeedLimit    = float(mydict["pitspeedlimit"])

                ppl = cr.ConfigReader.get_array_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_track_pitlane(self.track, self.layout, "l"))
                ppr = cr.ConfigReader.get_array_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_track_pitlane(self.track, self.layout, "r"))
                
                if len(ppl) != len(ppr):
                    print("number of pitlane definition points for left and right side are not equal.")
                    return
                 
                self.ptria = []
                for i in range(len(ppl)-1):
                    # triangle l1-l2-r1
                    self.ptria.append(tria.Tria(ppl[i][0], ppl[i][1], ppl[i+1][0], ppl[i+1][1], ppr[i][0], ppr[i][1]))
                    # triangle r1-r2-l2
                    self.ptria.append(tria.Tria(ppr[i][0], ppr[i][1], ppr[i+1][0], ppr[i+1][1], ppl[i+1][0], ppl[i+1][1]))


    def in_pitlane(self, px, pz):
        for t in self.ptria:
            if t.point_in_tria(px, pz) == True:
                return True
        return False

