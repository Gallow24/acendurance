import ac
import acsys

PitLaneDuration = 0

appWindow =0
lbl_PitStatus=0
lbl_LapStatus=0
lbl_PitTime=0

lbl_world  = 0
lbl_worldx = 0
lbl_worldz = 0

btn_save = 0

btn_pl = 0
btn_pr = 0
btn_add = 0
btn_clear = 0
btn_write = 0
lbl_curr_pl = 0
lbl_curr_pr = 0
curr_pl = "000.000;000.000"
curr_pr = "000.000;000.000"
pointsl = []
pointsr = []

Posx = 0
Posy = 0
Posz = 0
#Lege Standardwert für PitStatus fest
pitStatusMem=1
pitStatusCur=1
PitTime=0

LapCountPE = 0 #PitEntry
LapLocked = 0


def acMain(ac_version):
    
    global appWindow, lbl_PitStatus, lbl_LapStatus, lbl_PitTime, lbl_world, lbl_worldx, lbl_worldz, Posx, Posz, btn_save, lbl_curr_pl, lbl_curr_pr
    appWindow = ac.newApp("ACEPitlaneConfig")
    ac.setSize(appWindow, 280, 320)

    lbl_PitStatus = ac.addLabel(appWindow, "status pit: in pitlane")
    ac.setPosition(lbl_PitStatus, 3, 30)
    
    lbl_LapStatus = ac.addLabel(appWindow, "status lap: not completed")
    ac.setPosition(lbl_LapStatus, 3, 50)
    
    lbl_PitTime = ac.addLabel(appWindow, "0.0")
    ac.setPosition(lbl_PitTime, 200, 30)
    
    lbl_world = ac.addLabel(appWindow, "world position:")
    ac.setPosition(lbl_world, 3, 70)
    
    lbl_worldx = ac.addLabel(appWindow, "x: 0.000")
    ac.setPosition(lbl_worldx, 110, 70)
    
    lbl_worldz = ac.addLabel(appWindow, "z: 0.000")
    ac.setPosition(lbl_worldz, 200, 70)
    
    btn_pl = ac.addButton(appWindow, "point left")
    ac.setPosition(btn_pl, 3, 120)
    ac.setSize(btn_pl, 112, 20)
    ac.addOnClickedListener(btn_pl, btn_pl_click)
    
    lbl_curr_pl = ac.addLabel(appWindow, curr_pl)
    ac.setPosition(lbl_curr_pl, 3, 145)
    
    btn_pr = ac.addButton(appWindow, "point right")
    ac.setPosition(btn_pr, 155, 120)
    ac.setSize(btn_pr, 112, 20)
    ac.addOnClickedListener(btn_pr, btn_pr_click)
    
    lbl_curr_pr = ac.addLabel(appWindow, curr_pr)
    ac.setPosition(lbl_curr_pr, 155, 145)
    
    btn_add = ac.addButton(appWindow, "add pair")
    ac.setPosition(btn_add, 76, 190)
    ac.setSize(btn_add, 112, 20)
    ac.addOnClickedListener(btn_add, btn_add_click)
    
    btn_save = ac.addButton(appWindow, "save")
    ac.setPosition(btn_save, 76, 250)
    ac.setSize(btn_save, 112, 20)
    ac.addOnClickedListener(btn_save, Save_click)
    
    return "ACEPitlaneConfig"
    
    
def acUpdate(deltaT):
    global lbl_PitStatus, lbl_LapStatus, lbl_PitTime, lbl_world, lbl_worldx, lbl_worldz, pitStatusCur, pitStatusMem, PitTime, LapCountPE, LapLocked, Posx, Posy, Posz, PosStr
    
    Posx, Posy, Posz = ac.getCarState(0,acsys.CS.WorldPosition)
    
    ac.setText(lbl_worldx, "x: " + str(round(Posx,4)))
    ac.setText(lbl_worldz, "z: " + str(round(Posz,4)))

    pitStatusCur = ac.isCarInPitlane(0)
    if pitStatusCur == 1:
        if ac.getCarState(0,acsys.CS.LapCount) > LapCountPE:
            MsgStatusLap = "status lap: completed"
        else:
            MsgStatusLap = "status lap: not completed"
    
    else:
        MsgStatusLap = "status lap: on track"
        
    ac.setText(lbl_LapStatus, MsgStatusLap)
    
        
    if pitStatusCur != pitStatusMem:
        pitStatusMem = pitStatusCur
        MsgStatusPit = "status pit: " + DecodeStatus(pitStatusCur)
        ac.setText(lbl_PitStatus, MsgStatusPit)
        
        # Wenn Einfahrt in Box setze Zeit zurück
        if pitStatusCur == 1:
            PitTime = 0
            
    # Während Auto in Pitlane ist
    if pitStatusCur == 1:
        PitTime = PitTime + deltaT
    
    ac.setText(lbl_PitTime, str(round(PitTime, 1)))
        

def btn_pl_click(*args):
    global curr_pl, lbl_curr_pl
    curr_pl = str(round(Posx, 4)) + ";" + str(round(Posz, 4))
    ac.setText(lbl_curr_pl, curr_pl)
    ac.console(curr_pl)
    
def btn_pr_click(*args):
    global curr_pr, lbl_curr_pr
    curr_pr = str(round(Posx, 4)) + ";" + str(round(Posz, 4))
    ac.setText(lbl_curr_pr, curr_pr)
    ac.console(curr_pr)
    
def btn_add_click(*args):
    global curr_pl, curr_pr, pointsl, pointsr, lbl_curr_pl, lbl_curr_pr
    if curr_pl != curr_pr and curr_pl != "000.000;000.000" and curr_pr != "000.000;000.000":
        pointsl.append(curr_pl)
        pointsr.append(curr_pr)
        curr_pl = "000.000;000.000"
        curr_pr = "000.000;000.000"
        ac.setText(lbl_curr_pl, curr_pl)
        ac.setText(lbl_curr_pr, curr_pr)
        ac.console("points added")
    else:
        ac.console("points not added")
    
    
def Save_click(*args):
    global btn_save, PitTime, points
    ac.setText(btn_save, "saved")

    XMLString = "PitPoints for " + ac.getTrackName(0)+ ":\n"
    XMLString += gen_xml_line_tagonly("track", 0, 0)
    XMLString += gen_xml_line("name", ac.getTrackName(0), 1)
    XMLString += gen_xml_line_tagonly("pitlane", 1, 0)
    for i in range(len(pointsl)):
        XMLString += gen_xml_line("pleft",  pointsl[i], 2, 0)
        XMLString += gen_xml_line("pright", pointsr[i], 0, 1)
    XMLString += gen_xml_line_tagonly("pitlane", 1, 1)
    #XMLString += gen_xml_line("drivethrough", str(round(PitTime,1)), 1)
    XMLString += gen_xml_line("pitspeedlimit", "400", 1)
    XMLString += gen_xml_line_tagonly("track", 0, 1)
    
    f = open('./apps/python/ACEPitlaneConfig/PitPoints.txt', 'a')
    f.write(XMLString)
    f.close()
    

def gen_xml_line_tagonly(tag, indentlevel, closetag=0):
    return "\t"*indentlevel + "<" + "/"*closetag + tag + ">\n"

def gen_xml_line(tag, value, indentlevel, appendnewline=1):
    return "\t" * indentlevel + "<" + tag + ">" + value + "</" + tag + ">" + "\n" * appendnewline
    
    
    
        
        
def DecodeStatus(StatusIn):
    if StatusIn == 1:
        return "in pitlane"
    else:
        return "on track"

    