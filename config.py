import datetime

import config_reader as cr

CONFIGFILE = "./config/general.xml"

class Ace_Config: #acendurance_config
    def __init__(self):
        self.StopMinTimeCount = 1
        self.StopMinTimeSeconds = 0
        self.StopPenaltyMode = 0 #0:disabled, 1:drivethrough + PenaltySeconds 2: drivethrough + PenaltySeconds + gainedtime
        self.StopPenaltySeconds = 10 #for stoppenalty mode 2
        self.SpeedingPenaltyMode = 0 #0:disabled, #1:drivethrough, 2#: stop-and-go, 3: add to pit time for valid pitstop
        self.SendDrivernames = 0
        self.read_configfile()

    def __repr__(self):
        return (f"{self.__class__.__name__}("
                f"{self.StopMinTimeCount}, {self.StopPenaltyMode}, {self.StopPenaltySeconds}, {self.SpeedingPenaltyMode})")


    def read_configfile(self):
        mydict = cr.ConfigReader.get_dictarray_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_ace_settings())[0]
        self.StopMinTimeCount       = int(mydict["stopmintimecount"])
        self.StopMinTimeSeconds     = int(mydict["stopmintimeseconds"])
        self.StopPenaltyMode        = int(mydict["stoppenaltymode"])
        self.StopPenaltySeconds     = int(mydict["stoppenaltyseconds"])
        self.SpeedingPenaltyMode    = int(mydict["speedingpenaltymode"])
        self.SendDrivernames        = int(mydict["senddrivernames"])

        

class ACServerConn:
    def __init__(self):
        self.IP_Sending = "127.0.0.1"
        self.Port_Rec = 12000
        self.Port_Send = 11000
        self.EnableTVInfo = 0
        self.TrackXMLOK = False
        self.DediConnected = False
        self.RealTimeOK = False
        self.car_id_refresh = 0
        self.read_configfile()
        

    def read_configfile(self):
        mydict = cr.ConfigReader.get_dictarray_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_acs_settings())[0]
        self.IP_Sending   = mydict["ip_sending"]
        self.Port_Rec     = int(mydict["port_rec"])
        self.Port_Send    = int(mydict["port_send"])
        