reamde - ACEndurance server plugin

IMPORTANT:
1. install lxml library to your python installation
	- run: pip install lxml
	- more information: https://pypi.org/project/lxml/
2. read the changelog and the hints in general.xml
3. run the server in pickup mode. this is essential to run the tool properly.


roadmap:
	-> tba

changelog:
	30.04.2021 - first update of ACEndurance
		-> fixed an issue with ingame messages not sent to correct car
		-> added PitPointConfiguration.pdf with instructions to configure track specific settings
		-> added track configuration for monza and silverstone

	
---------------------------------------------------------------------------------------------------------------------------------------------------------------

about the problems of assetto corsa and why to use the plugin:
	this is a plugin to keep fair racing during driver swaps. assetto corsa supports just a simple function leaving and joining the server again.
	the joined driver can leave the pits instantly after he has completed loading with full fuel and fresh tires. users with faster pc�s have an advantage.
	another problem persists with drivers pitting without a driver swap. it�s possible that a driver is faster with rejoining than doing a full service (full fuel and fresh tires).
	
what does the tool?
	when a driver enters the pitlane the tool starts a timer. if the driver now leaves the server the minimum stopage time becomes active.
	now the next driver joins the server and gets a countdown via ingame chat. when the countdown expires, the driver can leave the pitlane.
	if a driver leaves the pit too early, the team gets a penalty. the duration of this is variable. it�s the sum of the drive- through time plus the time he left the pitlane too early.
	so the net penalty is a drive through. a team has three laps to serve the penalty. if it is not served within this amount of laps the tool will kick the driver.
	
how to use
	to use the tool properly the server admin needs to prepare some things.
	
	tracks.xml:
		here you configure global coordinates of the pit entry and exit. the fairracing ingame app supports you.
		just follow the instructions in PitPointConfiguration.pdf

	cars_.xml & drivers.xml
		this file is needed for live timing functionality. this is not completely implemented yet.
		
	general.xml
		here you can edit the basic settings of the app
	
