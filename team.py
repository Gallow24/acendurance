import datetime
import math

class Team():
    """description of class"""
    default_datetime = datetime.datetime(2000, 1, 1)

    def __init__(self):
        self.car_id            = 0
        self.TimeEntry         = Team.default_datetime # Zeitpunkt Boxeneinfahrt, Standardwert wichtig
        self.TimeExitTarget    = Team.default_datetime # Berechneter Zeitpunkt der Ausfahrt
        self.TimePitLeftSec    = 0 # TimePitLeft in Sekunden, wenn negativ ist Mindestzeit vorbei

        self.InPitlane = True   # Fahrzeug in Pitlane?
        self.Swap      = False  # Findet ein Fahrerwechsel statt?
        self.Connected = False
        self.Disco     = False
        
        self.Penalty     = 0  # 0: no penalty / 1: penalty to serve / 2: speeding penalty registered
        self.PenaltyLaps = 0    # remaining laps to serve the penalty

        self.PitStopsCount    = 0 # count all stops
        self.PitStopsValid    = 0 # count stops with valid minimum time
        self.SwapCount        = 0 # count all swaps
        self.SwapCountValid   = 0 # count driverswaps with valid minimum time

        self.MsgNext = Team.default_datetime
        # car related data for later use in livetiming etc.
        self.CarName    = ""
        self.ClassName  = ""
        # team and driver related data
        self.Guid = ""
        self.GuidPrevious = "" # to detect driver swaps
        self.DriverName = ""
        self.TeamName = ""
        
    def __repr__(self):
        return (f"{self.__class__.__name__}:"
                f"car_id: {self.car_id} guid: {self.Guid} GuidPrevious: {self.GuidPrevious} DriverName: {self.DriverName} TeamName: {self.TeamName} "
                f"CarName: {self.CarName} ClassName: {self.ClassName} "
                f"InPitlane: {self.InPitlane} Swap: {self.Swap} Connected: {self.Connected} Disco: {self.Disco} Penalty: {self.Penalty} PenaltyLaps{self.PenaltyLaps} "
                f"PitStopsCount: {self.PitStopsCount} PitStopsValid: {self.PitStopsValid} SwapCount: {self.SwapCount} SwapCountValid: {self.SwapCountValid}")
                
    # TODO
    def reset_values(self):
        # values which should not be resetted commented out
        #self.car_id            = 0
        self.TimeEntry         = Team.default_datetime
        self.TimeExitTarget    = Team.default_datetime
        self.TimePitLeftSec    = 0
        #self.InPitlane = True
        self.Swap      = False
        #self.Connected = False
        self.Disco     = False
        self.Penalty     = 0
        self.PenaltyLaps = 0
        self.PitStopsCount    = 0 # count all stops
        self.PitStopsValid    = 0 # count stops with valid minimum time
        self.SwapCount        = 0 # count all swaps
        self.SwapCountValid   = 0 # count driverswaps with valid minimum time
        self.MsgNext = Team.default_datetime
        #self.CarName    = ""
        #self.ClassName  = ""
        # team and driver related data
        #self.Guid = ""
        self.GuidPrevious = ""
        #self.DriverName = ""
        #self.TeamName = ""
        
    def setSwapState(self):
        if self.Guid != self.GuidPrevious and self.GuidPrevious != "":
            self.Swap = True
        else:
            self.Swap = False
    

    @staticmethod # static, because we will move this later to another module
    def timedelta_to_seconds(timedelta):
        return timedelta.days * 86400 + timedelta.seconds + timedelta.microseconds / 1000000 # a day has 86400 seconds
    

    def calc_CurrPitStop(self):
        TimePitLeft = self.TimeExitTarget - datetime.datetime.now()
        self.TimePitLeftSec = Team.timedelta_to_seconds(TimePitLeft)

    def send_next_msg(self):
        if datetime.datetime.now() > self.MsgNext and self.TimeExitTarget != Team.default_datetime:
            if self.TimePitLeftSec > 10:
                self.MsgNext = datetime.datetime.now() + datetime.timedelta(0, self.TimePitLeftSec%5)
            elif self.TimePitLeftSec < - 5:
                self.MsgNext = datetime.datetime.now() + datetime.timedelta(1) # just add one day to disable messages
            else:
                self.MsgNext = datetime.datetime.now() + datetime.timedelta(0, self.TimePitLeftSec%1)

            if self.Penalty > 0 and self.InPitlane == False:
                self.MsgNext = datetime.datetime.now() + datetime.timedelta(0, 15)
            return True
        return False
