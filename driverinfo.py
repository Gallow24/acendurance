import config_reader as cr

CONFIGFILE = "./config/drivers.xml"

class DriverInfo(object):
    def __init__(self):
        self.guid = ""
        self.name = ""

    def __repr__(self):
        return (f"{self.__class__.__name__}("
                f"{self.guid}, {self.name})")


    @staticmethod
    def read_configfile():
        #configreader returns an array of dicts. in this case we need a simple dict.
        mydict = cr.ConfigReader.get_dictarray_from_xpath(CONFIGFILE, cr.ConfigReader.gen_xpath_driver())
        retdict = {}
        for d in mydict:
            retdict[d["guid"]] = d["name"]

        return retdict


