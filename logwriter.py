import datetime

LOGFOLDER = "./log/"
LOGNAMEFORMAT = "%Y%m%d%H%M%S"
TSFORMAT = "%Y.%m.%d %H:%M:%S"
SEPARATOR = ";"
MSGT_PITSTOP = "Pitstop"
MSGT_INFO = "Info"
MSGT_WARN = "Warning"
MSGT_INFR = "Infringement"

class LogWriter(object):

    def __init__(self):
        self.filename = ""
        self._gen_filename()
        self.lines = []
        self._write_ace_logheader()

    def writeline(self, message, timestamp=True):
        f = open(LOGFOLDER + self.filename, "a+")
        writemsg = message + "\r"
        if timestamp:
            writemsg = datetime.datetime.now().strftime(TSFORMAT) + " " + message + "\r"
        self.lines.append(writemsg)
        try:
            f.writelines(self.lines)
            f.close()
            self.lines.clear()
        except:
            None

    def _write_csv_line(self, args):
        f = open(LOGFOLDER + self.filename, "a+")
        line = ""
        for a in args:
            line += a + SEPARATOR
        line += "\r"
        self.lines.append(line)
        try:
            f.writelines(self.lines)
            f.close()
            self.lines.clear()
        except:
            None

    def write_ace_logline(self, car_id, teamname, drivername, msgtype, message, pitstopseconds=0.0, mintimeok=0, driverswap=0):
        args = [datetime.datetime.now().strftime(TSFORMAT), f"{car_id}", teamname, drivername, msgtype, message,
                f"{pitstopseconds:.1f}", f"{mintimeok}", f"{driverswap}"]
        self._write_csv_line(args)

    def _write_ace_logheader(self):
        args = ["timestamp", "car_id", "Team", "Driver", "MsgType", "Message", "PitStopSeconds", "MinTimeOK", "Driverswap"]
        self._write_csv_line(args)

    def _gen_filename(self):
        self.filename = datetime.datetime.now().strftime(LOGNAMEFORMAT)+".txt"
