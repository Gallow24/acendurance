import codecs
import socket
import struct
import sys
import datetime
import math

import proto
import team
import driverinfo
import carinfo
import trackinfo
import we_session
import config
import logwriter

if sys.version_info[0] != 3:
    print('This program requires Python 3')
    sys.exit(1)

SUPPORTED_PROTOCOLS = [2, 4]


class Vector3f(object):
    def __init__(self):
        self.x = 0
        self.y = 0
        self.z = 0

    def __str__(self):
        return '[%f, %f, %f]' % (self.x, self.y, self.z)


class BinaryReader(object):
    def __init__(self, data):
        self.data = data
        self.offset = 0

    def read_byte(self):
        fmt = 'B'
        r = struct.unpack_from(fmt, self.data, self.offset)[0]
        self.offset += struct.calcsize(fmt)
        return r

    def read_bytes(self, length):
        fmt = '%dB' % length
        r = struct.unpack_from(fmt, self.data, self.offset)
        self.offset += struct.calcsize(fmt)
        return r

    def read_int32(self):
        fmt = 'i'
        r = struct.unpack_from(fmt, self.data, self.offset)[0]
        self.offset += struct.calcsize(fmt)
        return r

    def read_single(self):
        fmt = 'f'
        r = struct.unpack_from(fmt, self.data, self.offset)[0]
        self.offset += struct.calcsize(fmt)
        return r

    def read_string(self):
        length = self.read_byte()
        bytes_str = self.read_bytes(length)
        return ''.join([chr(x) for x in bytes_str])

    def read_uint16(self):
        fmt = 'H'
        r = struct.unpack_from(fmt, self.data, self.offset)[0]
        self.offset += struct.calcsize(fmt)
        return r

    def read_uint32(self):
        fmt = 'I'
        r = struct.unpack_from(fmt, self.data, self.offset)[0]
        self.offset += struct.calcsize(fmt)
        return r

    def read_utf_string(self):
        length = self.read_byte()
        bytes_str = self.read_bytes(length * 4)

        return bytes(bytes_str).decode('utf-32')

    def read_vector_3f(self):
        v = Vector3f()
        v.x = self.read_single()
        v.y = self.read_single()
        v.z = self.read_single()

        return v


class BinaryWriter(object):
    # https://docs.python.org/3/library/struct.html#format-strings
    def __init__(self):
        self.buff = b''

    def write_byte(self, data):
        self.buff += struct.pack('B', data)

    def write_bytes(self, data, length):
        self.buff += struct.pack('%dB' % length, *data)

    def write_uint16(self, data):
        self.buff += struct.pack('H', data)

    def write_int16(self, data):
        self.buff += struct.pack('h', data)

    def write_utf_string(self, data):
        self.write_byte(len(data))

        bytes_str = data.encode('utf-32')
        if bytes_str.startswith(codecs.BOM_UTF32):
            # Remove the BOM
            bytes_str = bytes_str[len(codecs.BOM_UTF32):]
        self.write_bytes(bytes_str, len(bytes_str))


class Pserver(object):
    def __init__(self):
        self.br = None
        self.ACServerConn = config.ACServerConn() # init and read from general.xml
        self.ace_config   = config.Ace_Config()         # init read from general.xml
        self.Drivers      = driverinfo.DriverInfo.read_configfile() #dict with key=GUID and value=Name
        self.Cars         = carinfo.CarInfo.get_cars_dict() # dict with key=carname and value=carinfo-object
        self.log          = logwriter.LogWriter()
        self.Session      = we_session.WeSession()
        self.Trackinfo    = trackinfo.Trackinfo()

        self.Teams = []
        for i in range(64):
            self.Teams.append(team.Team())
            self.Teams[i].car_id = i #Bugfix: car_id was not initialized


    def _check_protocol(self, protocol_version):
        '''
        Checks that the given procotol version is supported,
        the program will quit otherwise
        '''
        if protocol_version not in SUPPORTED_PROTOCOLS:
            print('Unsupported protocol version: %d, expecting: %s' %
                  (protocol_version, SUPPORTED_PROTOCOLS))
            sys.exit(1)


    def _send(self, buff):
        '''
        Send buffer to server
        '''
        self.sock.sendto(buff, (self.ACServerConn.IP_Sending, self.ACServerConn.Port_Send))

    # The methods below are to handle data received from the server
    # the server sends a separate package for each car, so we have to think about socket.timeout
    def _handle_car_info(self):
        car_id = self.br.read_byte()
        is_connected = self.br.read_byte() != 0
        car_model = self.br.read_utf_string()
        car_skin = self.br.read_utf_string()
        driver_name = self.br.read_utf_string()
        driver_team = self.br.read_utf_string()
        driver_guid = self.br.read_utf_string()

        print('====')
        print('Car info: %d %s (%s), Driver: %s, Team: %s, GUID: %s, Connected: %s' %
              (car_id, car_model, car_skin, driver_name, driver_team, driver_guid, is_connected))
        
        self.Teams[car_id].TeamName = driver_team
        self.Teams[car_id].Guid = driver_guid
        self.Teams[car_id].DriverName = self.Drivers.get(driver_guid, "drivername notfound")
        self.Teams[car_id].CarName = car_model
        
        car = self.Cars.get(car_model, carinfo.CarInfo()) # try to get the car, default value is a new carinfo-object with its default values
        #self.Teams[car_id].PitCrewTime = car.PitCrewTime
        self.Teams[car_id].ClassName  = car.RaceClass


    def _handle_car_update(self):
        car_id = self.br.read_byte()
        pos = self.br.read_vector_3f()
        velocity = self.br.read_vector_3f()
        gear = self.br.read_byte()
        engine_rpm = self.br.read_uint16()
        normalized_spline_pos = self.br.read_single()
        
        # speicher aktuelle Werte, um bei Abbruch der Session eine korrekte Neutralisation durchführen zu können.
        # self.Teams[car_id].car_id = car_id
        # self.Teams[car_id].SplinePos = normalized_spline_pos
        # self.Teams[car_id].PosX = pos.x
        # self.Teams[car_id].PosZ = pos.z
        #print(f"{normalized_spline_pos}")

        speed = round(math.sqrt(velocity.x**2 + velocity.z**2) * 3.6, 0)
        # prüfe, ob das Auto verbunden ist / Bugfix: Jedes Auto bekommt ein ASCP_CAR_UPDATE, auch wenn client nicht verbunden ist, daher diese Prüfung
        if speed > 10:
            self.Teams[car_id].Connected = True
        # if car stops in Pitlane it can not solve a drivethrough-penalty.
        if speed < 3 and self.Teams[car_id].Penalty == 1 and self.Teams[car_id].InPitlane == True:
            self.Teams[car_id].Penalty = 2

        inpit = self.Trackinfo.in_pitlane(pos.x, pos.z)

        # pit entry
        if inpit == True:
            if self.Teams[car_id].InPitlane == False: # if event was´nt registered yet
                self.Teams[car_id].InPitlane = True
                self.Teams[car_id].Disco = False # wenn der Fahrer selbst in die Box fährt, schließe einen Disconnect aus
                self.Teams[car_id].TimeEntry = datetime.datetime.now()
                self._send_chat(car_id, "entered pitlane")
                self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFO, "entered pitlane")
                
                # Speeding in Penalty: allow team to take the pitstop and penalize them afterwards with a drivethrough
                if speed > self.Trackinfo.PitSpeedLimit and self.ace_config.SpeedingPenaltyMode > 0:
                    self._send_chat(car_id, "speeding in pitlane")
                    self.Teams[car_id].Penalty = 2
                    self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFR, "speeding in pitlane")

                if self.Teams[car_id].Penalty == 1: #Penalty value is set to 1 in pit exit
                    self._send_chat(car_id, "Drive through to serve the penalty!")
                # calculate time for pitexit
                self.Teams[car_id].TimeExitTarget = self.Teams[car_id].TimeEntry + datetime.timedelta(0, self.ace_config.StopMinTimeSeconds)
        # Ausfahrt Box
        else:
            if self.Teams[car_id].InPitlane == True:
                self.Teams[car_id].InPitlane = False
                self.Teams[car_id].calc_CurrPitStop() #berechnet TimePitLeft, Curr_PitStop_Valid
                
                # was it a disconnect?
                if self.Teams[car_id].Disco == True:
                    self.Teams[car_id].Disco == False # reset disconnect value
                    self._send_chat(car_id, f"Disconnect detected. No Stop with minimum time accepted.")
                    self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFR, "Disconnect detected before pitstop. Pitstop not accepted", self.Teams[car_id].TimePitLeftSec)
                    if self.Teams[car_id].Swap == True:
                        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFR, "Driverswap after disconnect", self.Teams[car_id].TimePitLeftSec)
                        self.Teams[car_id].PitStopsCount += 1
                        self.Teams[car_id].SwapCount += 1 # increase number of swaps independent of disconnect
                        self.Teams[car_id].Swap = False # reset the value
                    return # no further actions
                    
                    # nur wenn kein Disco vorlag, kann ein gültiger Stopp durchgeführt werden
                if self.Teams[car_id].Penalty == 2:  # driver got a penalty for speeding on entry. now on exit set it´s state to 1
                    self.Teams[car_id].Penalty = 1
                    self.Teams[car_id].PenaltyLaps = 3
                elif self.Teams[car_id].Penalty == 1: # driver went to pit to serve the penalty
                    self._send_chat(car_id, "Penalty served")
                    self.Teams[car_id].Penalty = 0
                    self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFR, "Penalty served")
                    return # if penalty is served, no other actions are possible
                else:
                    self.Teams[car_id].PitStopsCount += 1 # increase number of pitstops
                    
                # valid pitstop-time
                if self.Teams[car_id].TimePitLeftSec <= 0 and self.Teams[car_id].TimeEntry != team.Team.default_datetime:
                    self.Teams[car_id].PitStopsValid += 1
                    
                    if self.Teams[car_id].Swap == True:
                        self._send_chat(car_id, f"Driverswap ok. Valid stops: {self.Teams[car_id].PitStopsValid} / {self.ace_config.StopMinTimeCount}")
                        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_PITSTOP, "Driverswap ok", self.Teams[car_id].TimePitLeftSec, 1, 1)
                        self.Teams[car_id].Swap = False # setze den Wert für den Fahrerwechsel wieder zurück
                    else:
                        self._send_chat(car_id, f"Pitstop ok. Valid stops: {self.Teams[car_id].PitStopsValid} / {self.ace_config.StopMinTimeCount}")
                        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_PITSTOP, "Pitstop ok", self.Teams[car_id].TimePitLeftSec, 1, 1)
                            
                    #self.Teams[car_id].TimePitLeftSec = 0 # setze die Zeit für restliche Zeit zurück

                
                else: # TimePitLeft >= 0: Zeit in Boxengasse war zu kurz
                    if self.Teams[car_id].Swap == True:
                        self._broadcast_chat("car" + self.Teams[car_id].TeamName + " left pit too early after driverswap")
                        self._send_chat(car_id, "Left Pit too early during driverswap!")
                        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFR, "PitStop with driverswap too short.", self.Teams[car_id].TimePitLeftSec, 0, 1)
                    else:
                        self._send_chat(car_id, f"Pitstop without minimum time. Valid stops: {self.Teams[car_id].PitStopsValid} / {self.ace_config.StopMinTimeCount}")
                        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_PITSTOP, "Short Pitstop without minimum time.", self.Teams[car_id].TimePitLeftSec, 0, 0)
                
                if self.Teams[car_id].Swap == True:
                    self.Teams[car_id].SwapCount += 1
                    self.Teams[car_id].Swap = False

    def _handle_chat(self):
        car_id = self.br.read_byte()
        msg = self.br.read_utf_string()
        print('====')
        print('Chat from car %d: "%s"' % (car_id, msg))

        if msg == "++help":
            self._send_chat(car_id, "ingame- commands for fairracing:")
            self._send_chat(car_id, "++tvinfo_enable - enable notifications for all cars")
            self._send_chat(car_id, "++tvinfo_disable - disable notifications for all cars")
            self._send_chat(car_id, "++reload_trackinfo - reloads the pitpoints.xml file")
            self._send_chat(car_id, "++get_drivernames - sends you the current driver names")
        elif msg == "++reload_trackinfo":
            self._enable_realtime_report()
            self._send_chat(car_id, "admin command accepted")
            self.ACServerConn.TrackXMLOK = False # program tries to find the trackname again and find the xml-parameters
        elif msg == "++carinfo":
            print(self.Teams[car_id])
        elif msg == "++get_drivernames":
            self._send_drivernames(car_id, False)
         
         #elif msg == "++get_drivernames":
         #  self.Send_All_Drivernames(car_id) # Funktion noch nicht vorhanden


    def _handle_client_event(self):
        event_type = self.br.read_byte()
        car_id = self.br.read_byte()
        other_car_id = 255

        if event_type == proto.ACSP_CE_COLLISION_WITH_CAR:
            other_car_id = self.br.read_byte()
        elif event_type == proto.ACSP_CE_COLLISION_WITH_ENV:
            pass

        impact_speed = self.br.read_single()
        world_pos = self.br.read_vector_3f()
        rel_pos = self.br.read_vector_3f()

        print('====')
        if event_type == proto.ACSP_CE_COLLISION_WITH_CAR:
            print('Collision with car, car: %d, other car: %d, Impact speed: %f, World position: %s, Relative position: %s' %
                  (car_id, other_car_id, impact_speed, world_pos, rel_pos))
        elif event_type == proto.ACSP_CE_COLLISION_WITH_ENV:
            print('Collision with environment, car: %d, Impact speed: %f, World position: %s, Relative position: %s' %
                  (car_id, impact_speed, world_pos, rel_pos))

    def _handle_client_loaded(self):
        car_id = self.br.read_byte()
        print('====')
        print('Client loaded: %d' % car_id)
        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_PITSTOP, "client loaded on server")
        
        # Fallunterscheidung:
        # 1. Connection_Closed ausgelöst: Disconnect wird erkannt. Connected wird dort false, Disco wird true
        # 2. Connection_Closed nicht ausgelöst:
        #         - Connected ist noch immer true, da das Event nicht getriggert wurde
        #         - nur wenn InPitLane == false ist, soll der Disconnect bewertet werden. Hat ein Fahrer zufällig einen Disco in der Box, soll er keine Strafe dafür bekommen
        
        #if self.Teams[0].Connected == True and self.Teams[car_id].InPitlane == False:
        #    self.Teams[car_id].Disco = True

        self.Teams[car_id].Connected = True

        # wenn Session_active = false ist die Session wirklich nicht aktiv oder der Wert wurde nicht aktualisiert
        if self.Session.active == False:
            self.Session.Refresh_Session_Active()

        #if self.Session.active == False and Session.type == 3: #Rennsession nicht aktiv bedeutet, dass Auto in Startaufstellung steht
        #    self.Teams[car_id].InPitlane = False
        #else:
        #    self.Teams[car_id].InPitlane = True

        if self.ace_config.SendDrivernames > 0:
            self._send_drivernames(car_id)

        self.Teams[car_id].calc_CurrPitStop()
        if self.Teams[car_id].Disco == True:
            self._send_chat(car_id, "disconnect detected. pitstop will not be accepted.")
        elif self.Teams[car_id].TimePitLeftSec <= 0:
            self._send_chat(car_id, "pit time ok. you can leave the pits now!")


    def _handle_connection_closed(self):
        driver_name = self.br.read_utf_string()
        driver_guid = self.br.read_utf_string()
        car_id = self.br.read_byte()
        car_model = self.br.read_string()
        car_skin = self.br.read_string()

        print('====')
        print('Connection closed')
        print('Driver: %s, GUID: %s' % (driver_name, driver_guid))
        print('Car: %d, Model: %s, Skin: %s' % (car_id, car_model, car_skin))

        self.log.write_ace_logline(car_id, self.Teams[car_id].TeamName, self.Teams[car_id].DriverName, logwriter.MSGT_INFO, "left server")
        self.Teams[car_id].GuidPrevious = driver_guid; # save this guid as previous to detect driver swap
                            
        # wenn das Auto beim Verlassen des Servers nicht in der Boxengasse ist, hat der Fahrer die Box nicht ordnungsgemäß betreten.
        if self.Teams[car_id].InPitlane == False:
            self.Teams[car_id].Disco = True
        else:
            self.Teams[car_id].Disco = False

        # wenn der Fahrer den Server ordnungsgemäß verlässt, registriere den Verbindungsstatus. Wichtig für Erkennung eines Disconnects.
        self.Teams[car_id].Connected = False

        # Unabh. von weiteren Ereignissen ist das Fahrzeug nun in jedem Fall in der Box
        self.Teams[car_id].InPitlane = True


    def _handle_end_session(self):
        filename = self.br.read_utf_string()
        print('====')
        print('Report JSON available at: %s' % filename)

    def _handle_error(self):
        print('====')
        print('ERROR: %s' % self.br.read_utf_string())

    def _handle_lap_completed(self):
        car_id = self.br.read_byte()
        laptime = self.br.read_uint32()
        cuts = self.br.read_byte()

        print('====')
        print('Lap completed')
        print('Car: %d, Laptime: %d, Cuts: %d' % (car_id, laptime, cuts))

        cars_count = self.br.read_byte()

        for i in range(1, cars_count + 1):
            rcar_id = self.br.read_byte()
            rtime = self.br.read_uint32()
            rlaps = self.br.read_byte()
            print('%d: Car ID: %d, Time: %d, Laps: %d' %
                  (i, rcar_id, rtime, rlaps))

        grip_level = self.br.read_byte()
        print('Grip level: %d' % grip_level)

        # Strafen - reduziere die Anzahl der noch möglichen Runden zum Absitzen der Strafe
        if self.Teams[car_id].Penalty > 0 and self.Teams[car_id].InPitlane == False:
            self.Teams[car_id].PenaltyLaps -= 1

        if self.Teams[car_id].Penalty > 0 and self.Teams[car_id].PenaltyLaps < 0:
            #testKick(client, car_id);
            self._send_chat(car_id, "race control will investigate this after the race");
            self.Teams[car_id].Penalty = 0
        
        if self.Teams[car_id].InPitlane == True:
            self._send_chat(car_id, "lap completed");
        


    def _handle_new_connection(self):
        driver_name = self.br.read_utf_string()
        driver_guid = self.br.read_utf_string()
        car_id = self.br.read_byte()
        car_model = self.br.read_string()
        car_skin = self.br.read_string()

        print('====')
        print('New connection')
        print('Driver: %s, GUID: %s' % (driver_name, driver_guid))
        print('Car: %d, Model: %s, Skin: %s' % (car_id, car_model, car_skin))

        self.Teams[car_id].DriverName = self.Drivers.get(driver_guid, "drivername notfound")
        self.Teams[car_id].car_id = car_id
        self.Teams[car_id].Guid = driver_guid
        self.Teams[car_id].CarName = car_model

        self.Teams[car_id].setSwapState()
        
        self._get_car_info(car_id)


    def _handle_new_session(self):
        print('====')
        print('New session started')
        self.log._gen_filename() #generate a new log-file

        for Team in self.Teams:
            Team.reset_values()
            
        # stelle sicher, dass Echtzeit- Daten vom Server gesendet werden. Bug: Problemverhalten nicht genau reproduziert. - noch vorhanden?
        self._enable_realtime_report()
        
    def _handle_session_info(self):
        protocol_version = self.br.read_byte()
        session_index = self.br.read_byte()
        current_session_index = self.br.read_byte()
        session_count = self.br.read_byte()
        server_name = self.br.read_utf_string()
        track = self.br.read_string()
        track_config = self.br.read_string()
        name = self.br.read_string()
        typ = self.br.read_byte()
        time = self.br.read_uint16()
        laps = self.br.read_uint16()
        wait_time = self.br.read_uint16()
        ambient_temp = self.br.read_byte()
        road_temp = self.br.read_byte()
        weather_graphics = self.br.read_string()
        elapsed_ms = self.br.read_int32()

        self._check_protocol(protocol_version)
        self.Session.active_since = datetime.datetime.now() + datetime.timedelta(0, 0, 0, -elapsed_ms) #currentMS ist während WaitTime negativ, daher kehre das Vorzeichen um
        self.Session.type = typ # Index in Session schreiben, um Tool für bestimmte Sessions steuern zu können
        self.Session.Refresh_Session_Active() # Bestimme, ob wir uns in der Wartezeit befinden oder bereits innerhalb der Session
        
        
        if track != self.Trackinfo.track or track_config != self.Trackinfo.layout:
            self.Trackinfo.track = track
            self.Trackinfo.layout = track_config
            self.Trackinfo.gen_trias()
            if len(self.Trackinfo.ptria) > 0:
                self.ACServerConn.TrackXMLOK = True

        self.ACServerConn.DediConnected = True


    def _handle_version(self):
        protocol_version = self.br.read_byte()
        self._check_protocol(protocol_version)
        print('====')
        print('Protocol version: %d' % protocol_version)

    # The methods below are to send data to the server

    def _admin_command(self, command):
        '''
        Send admin command
        '''
        bw = BinaryWriter()

        bw.write_byte(proto.ACSP_ADMIN_COMMAND)
        bw.write_utf_string(command)

        self._send(bw.buff)

    def _broadcast_chat(self, message):
        '''
        Broadcast message to all cars
        '''
        bw = BinaryWriter()

        bw.write_byte(proto.ACSP_BROADCAST_CHAT)
        bw.write_utf_string(message)

        self._send(bw.buff)

    def _get_car_info(self, car_id):
        '''
        Requests car info for car_id
        '''
        bw = BinaryWriter()

        bw.write_byte(proto.ACSP_GET_CAR_INFO)
        bw.write_byte(car_id)

        self._send(bw.buff)

    def _get_session_info(self):
        '''
        Requests session info
        '''
        bw = BinaryWriter()

        bw.write_byte(proto.ACSP_GET_SESSION_INFO)
        bw.write_int16(-1) # Session index (-1 to request the current session)

        self._send(bw.buff)


    def _enable_realtime_report(self, interval=100):
        bw = BinaryWriter()
        bw.write_byte(proto.ACSP_REALTIMEPOS_INTERVAL)
        bw.write_uint16(interval)  # Interval in ms

        self._send(bw.buff)

    def _kick(self, car_id):
        bw = BinaryWriter()
        bw.write_byte(proto.ACSP_KICK_USER)
        bw.write_byte(car_id)

        self._send(bw.buff)

    def _next_session(self):
        bw = BinaryWriter()
        bw.write_byte(proto.ACSP_NEXT_SESSION)

        self._send(bw.buff)

    def _restart_session(self):
        bw = BinaryWriter()
        bw.write_byte(proto.ACSP_RESTART_SESSION)

        self._send(bw.buff)

    def _send_chat(self, car_id, message):
        '''
        Send message to specific car
        '''
        bw = BinaryWriter()

        bw.write_byte(proto.ACSP_SEND_CHAT)
        bw.write_byte(car_id)
        bw.write_utf_string(message)

        self._send(bw.buff)


#-------------------------------------------------------------------------------------------------------------------
    def _messaging(self):
        for Team in self.Teams:
            if Team.Connected == False:
                continue                
            if Team.InPitlane == False and Team.Penalty == 0:
                continue
            Team.calc_CurrPitStop()
            if Team.send_next_msg() == False:
                continue
                
            if Team.InPitlane == True:
                print(str(Team.TimePitLeftSec)) #debug
                if Team.Penalty != 1:
                    if Team.TimePitLeftSec > 0:
                        self._send_chat(Team.car_id, f"Remaining Pitlane Time: {Team.TimePitLeftSec:.0f}s")
                    else:
                        self._send_chat(Team.car_id, "go")                        

            # wenn Strafe vorhanden und Fahrer auf der Strecke ist
            if Team.InPitlane == False and Team.Penalty == 1:   # show team on track it has to serve the penalty
                self._send_chat(Team.car_id, f"You have {Team.PenaltyLaps} laps left to serve the penalty.")


    def _send_drivernames(self, car_id, to_all = True):
        #1 send all drivernames to new driver
        linames = [None for r in range(len(self.Teams))]
        for i in range(len(self.Teams)):
            linames[i] = self.Teams[i].DriverName
        linames.insert(0, linames.pop(car_id)) #move name of this driver to first position, because in AC driver 0 is always the own car
        for i in range(len(self.Teams)):
            if self.Teams[i].DriverName != "":
                self._send_chat(car_id, f"__{i}__{self.Teams[i].DriverName}")

        if not to_all:
            return
        #2 send new drivername to all other connected drivers
        for i in range(len(self.Teams)):
            if self.Teams[i].Connected and i != car_id:
                id = car_id
                if i > car_id:
                    id = car_id + 1
                self.send_chat(i, f"__{id}__{self.Teams[car_id].DriverName}")

        
    def _misc_jobs(self):
        # prüfe stets, ob alle verbundenen Autos einen Fahrernamen und eine GUID haben
        if self.ACServerConn.car_id_refresh >= len(self.Teams):
            self.ACServerConn.car_id_refresh = 0 # car_id_refresh zurücksetzen
        if self.Teams[self.ACServerConn.car_id_refresh].Guid == "" and self.Teams[self.ACServerConn.car_id_refresh].Connected == True:
            self._get_car_info(self.ACServerConn.car_id_refresh)
        self.ACServerConn.car_id_refresh += 1 # erhöhen, damit in nächster Iteration ein anderes Auto untersucht wird

        # wenn die Streckendaten nicht geladen wurden, hole das nach
        if self.ACServerConn.TrackXMLOK == False:
            self._get_session_info()
            # self.Trackinfo.init_refpoints()
            # if self.Trackinfo.pin_rect.refpointx != 0:
            #     self.ACServerConn.TrackXMLOK = True
            None
        
#-------------------------------------------------------------------------------------------------------------------

    def run(self):

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(("", self.ACServerConn.Port_Rec))

        self.sock.settimeout(0.5) #max wait time for next package
        
        print('Waiting for data from %s:%s' % (self.ACServerConn.IP_Sending, self.ACServerConn.Port_Rec))

        self.Session.active_since = datetime.datetime.now() + datetime.timedelta(0, -10)
    
        # steuert den sekündlichen Takt, in dem Nachrichten gesendet werden
        udp_timeout = 0 # speichert die Zeit in ms, wie lange keine Daten vom Server kamen

        while True:
            self.ACServerConn.DediConnected = False
            udp_timeout += 0.5
            try:
                sdata = self.sock.recv(1024)  # buffer size is 1024 bytes
                self.ACServerConn.DediConnected = True
            except socket.timeout as e:
                err = e.args[0]
                #print(err)
            except socket.error as e:
                print("Fehler an socket")
            #else:
            #    print("Sonstiger Fehler an Socket")
                

            if udp_timeout > 5:
                self.ACServerConn.DediConnected = False
                self.ACServerConn.RealTimeOK = False
                self.ACServerConn.TrackXMLOK = False
                self._get_session_info()
                print("session info angefordert")
                udp_timeout = 0
            
            if self.ACServerConn.DediConnected == False:
                continue #Nächste Iteration
            else:
                udp_timeout = 0 #Wert zurücksetzen, wenn Daten empfangen werden
                self.ACServerConn.DediConnected = True
            
                
            # --------------------------------------------
            # Daten konnten gelesen werden
            # --------------------------------------------
            if self.ACServerConn.RealTimeOK == False:
                self._enable_realtime_report()
                self.ACServerConn.RealTimeOK = True


            self.br = BinaryReader(sdata)
            packet_id = self.br.read_byte()
            #print(f"{packet_id}")

            if packet_id == proto.ACSP_CLIENT_LOADED:
                self._handle_client_loaded()

            elif packet_id == proto.ACSP_ERROR:
                self._handle_error()
            elif packet_id == proto.ACSP_CHAT:
                self._handle_chat()
            
            elif packet_id == proto.ACSP_VERSION:
                self._handle_version()
            elif packet_id == proto.ACSP_NEW_SESSION:
                self._handle_new_session()
                self._handle_session_info()

                # Uncomment to enable realtime position reports
                # self._enable_realtime_report()
            elif packet_id == proto.ACSP_SESSION_INFO:
                self._handle_session_info()
            elif packet_id == proto.ACSP_END_SESSION:
                self._handle_end_session()
            elif packet_id == proto.ACSP_CLIENT_EVENT:
                self._handle_client_event()
            elif packet_id == proto.ACSP_CAR_INFO:
                self._handle_car_info()
            elif packet_id == proto.ACSP_CAR_UPDATE:
                self._handle_car_update()
            elif packet_id == proto.ACSP_NEW_CONNECTION:
                self._handle_new_connection()
            elif packet_id == proto.ACSP_CONNECTION_CLOSED:
                self._handle_connection_closed()
            elif packet_id == proto.ACSP_LAP_COMPLETED:
                self._handle_lap_completed()
            else:
                print('** UNKOWNN PACKET ID: %d' % packet_id)

            self._messaging()
            self._misc_jobs() #check of car_id and track-data


