import lxml.etree as ET

class ConfigReader:
    def __init__(self):
        None

    # receive array of dicts from xml-file and a given xpath
    @staticmethod
    def get_dictarray_from_xpath(filename, xpath):
        root = ET.parse(filename).xpath(xpath)
        myList = []
        for elem in root:
            dict = {}
            for child in elem:
                if child.tag is not ET.Comment:
                    dict[child.tag.lower()] = child.text    #dict with small characters
            myList.append(dict)
        return myList

    @staticmethod
    def get_dict_from_xpath(filename, xpath):
        root = ET.parse(filename).xpath(xpath)
        dict = {}
        for elem in root:
            dict[elem.tag.lower()] = elem.text
        return dict

    @staticmethod
    def get_array_from_xpath(filename, xpath):
        root = ET.parse(filename).xpath(xpath)
        arr = []
        for elem in root:
            #arr.append(elem.text.split(";"))
            arr.append([float(s) for s in elem.text.split(";")])
        return arr


    @staticmethod
    def gen_xpath_track(track):
        retVal = "//track[name='" + track + "'][1]/*[not(name()='pitlane')]" #XPath version unknown
        return retVal
    
    @staticmethod
    def gen_xpath_track_pitlane(track, layout, side="l"):
        pside = "pleft"
        if side != "l":
            pside = "pright"
        retval = "//track[name='" + track + "'][1]/pitlane/" + pside
        return retval

    @staticmethod
    def gen_xpath_car():
        retVal = "//car"
        return retVal

    @staticmethod
    def gen_xpath_driver():
        retVal = "//driver"
        return retVal

    @staticmethod
    def gen_xpath_acs_settings():
        retVal = "//acs_settings"
        return retVal

    @staticmethod
    def gen_xpath_ace_settings():
        retVal = "//ace_settings"
        return retVal
