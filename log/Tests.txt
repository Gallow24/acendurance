Tests for ACEndurance

[ok] - detection of pitstatus
[ok] - pitstops:
[ok]	- pitstop with minimum time
[ok]		- number stops ++
[ok]		- number stops with minimum time ++
[ok]	- pitstop without minimum time
[ok]		- number stops ++

[ok] - driverswap:
[ok]	- leave server and join again
[ok]	- leave server and join with another driver
[ok]	- number stops ++
[ok]	- number stops with minimum time ++
[ok]	- number swaps ++
[ok]	- number swaps_valid ++
[ok]	- when swap but no minimum time log this event
[ok]	- bug: pit exit after rejoin and time is over no message that stop was ok
[ok]	- bug: when stop with swap too short and next stop is a stop without swap and without minimum time wrong message is sent

[ok] - penalties:
[ok]	- award a penalty for speeding
[ok]	- serve a penalty
[ok]	- get a penalty and perform a normal pitstop
[ok]	- when serving penalty and speeding again award the penalty again

[ok] - send chat messages that countdown looks better

[ok] - __repr__ for team class

[ok] - consistent logging
[ok]	- timestamp
[ok]	- car_id
[ok]	- teamname
[ok]	- drivername
[ok]	- message type
[ok]	- message
[ok]	- pitstop duration
[ok]	- stop_ok
[ok]	- driverswap

[ok] - send drivernames via chat
[ok]	- new driver gets all names
[ok]	- all other connected drivers get only the new name
[ok]	- chat command for request drivernames

[td] - renew program logic
[ok]	- remove drivethroughtime from trackconfig
[ok]	- remove PitCrewTime from cars
		- ... tba