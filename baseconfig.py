import datetime

import config_reader as cr

CONFIGFILE = "./config/general.xml"

class ace_config: #acendurance_config
    def __init__(self):
        self.StopMinTimeForAll = False
        self.StopMinTimeCount = 1
        self.StopPenaltyMode = 0 #0:disabled, 1:enabled, 2: todo
        self.StopPenaltySeconds = 0
        self.SpeedPenaltyMode = 0
        self.SpeedPenaltySeconds = 0


    def read_configfilee(self):
        mydict = cr.config_reader.get_DictArray_from_xpath(CONFIGFILE, cr.config_reader.gen_xpath_ace_config())[0]
        self.StopMinTimeForAll = Dict["MinTimeForAll"]
        self.DisablePenalty = Dict["DisablePenalty"]
        self.EnableTVInfo = Dict["EnableTVInfo"]
        self.StopMinTimeCount = Dict["StopsWithMinimumTime"]



class ACServerConnection:
    def __init__(self):
        self.IP_Sending = '127.0.0.1'
        self.Port_Rec = '12000'
        self.Port_Send = '11000'
        self.Frequency = 18 # 18Hz
        self.TrackXMLOK = False
        self.DediConnected = False
        self.RealTimeOK = False
        self.Next_Send = datetime.datetime.now()
        self.car_id_refresh = 0
        self.EnableTVInfo = 0